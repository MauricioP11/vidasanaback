const request = require('supertest');
const server = require('../src/index.js');

describe('API Tests', () => {
    let userId;
    let productId;
    let alimentacionId;

    describe('User Routes', () => {
        it('GET /users should respond with status 200 and return a list of users', async () => {
            const response = await request(server).get('/users');
            expect(response.status).toBe(200);
        });

        it('POST /users should create a new user and respond with status 200', async () => {
            const userData = {
                usuario_nombre: 'Test User',
                usuario_email: 'test@example.com',
                usuario_password: 'password123'
            };
            const response = await request(server)
                .post('/users')
                .send(userData);
            expect(response.status).toBe(200);
        });

        it('POST /users/login should login a user and respond with status 200', async () => {
            const userData = {
                usuario_email: 'test@example.com',
                usuario_password: 'password123'
            };
            const response = await request(server)
                .post('/users/login')
                .send(userData);
            expect(response.status).toBe(200);
            userId = response.body[0].usuario_id;
        });

        it('PUT /users/:usuario_id should update an existing user and respond with status 200', async () => {
            const updatedUserData = {
                usuario_id: userId,
                usuario_nombre: 'Updated Test User',
                usuario_email: 'updated_test@example.com',
                usuario_password: 'newpassword123'
            };
            const response = await request(server)
                .put(`/users/${userId}`)
                .send(updatedUserData);
            expect(response.status).toBe(200);
        });

        it('DELETE /users/:usuario_id should delete an existing user and respond with status 200', async () => {
            const response = await request(server).delete(`/users/${userId}`);
            expect(response.status).toBe(200);
        });
    });

    describe('Product Routes', () => {
        it('GET /api/products/all should respond with status 200 and return a list of products', async () => {
            const response = await request(server).get('/api/products/all');
            expect(response.status).toBe(200);
            expect(Array.isArray(response.body)).toBe(true);
            if (response.body.length > 0) {
                productId = response.body[0].producto_id;
            }
        });

        it('POST /api/products/register should create a new product and respond with status 200', async () => {
            const productData = {
                producto_nombre: 'Nuevo Producto',
                producto_calorias: 100,
                producto_proteinas: 20,
                producto_grasas: 5,
                producto_carbohidratos: 30,
                usuario_id: userId
            };
            const response = await request(server)
                .post('/api/products/register')
                .send(productData);
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('insertId'); // Este línea se puede omitir si insertId no es necesario
            productId = response.body.insertId;
        });

        it('PUT /api/products/:producto_id should update an existing product and respond with status 200', async () => {
            const updatedProductData = {
                producto_nombre: 'Producto Actualizado',
                producto_calorias: 150,
                producto_proteinas: 25,
                producto_grasas: 7,
                producto_carbohidratos: 35,
                usuario_id: userId
            };
            const response = await request(server)
                .put(`/api/products/${productId}`)
                .send(updatedProductData);
            expect(response.status).toBe(200);
        });

        it('GET /api/products/:producto_id should respond with status 200 and return the product with the specified ID', async () => {
            const response = await request(server).get(`/api/products/${productId}`);
            expect(response.status).toBe(200);
            expect(response.body.length).toBe(1);
            expect(response.body[0].producto_id).toBe(productId);
        });
    });

    describe('Food Routes', () => {
        it('GET /api/food/ali/all should respond with status 200 and return a list of food entries', async () => {
            const response = await request(server).get('/api/food/ali/all');
            expect(response.status).toBe(200);
            expect(Array.isArray(response.body)).toBe(true);
            if (response.body.length > 0) {
                alimentacionId = response.body[0].alimentacion_id;
            }
        });

        it('POST /food/ali should create a new food entry and respond with status 200', async () => {
            const foodData = {
                alimentacion_fecha: '2024-05-12',
                producto_id: productId,
                alimentacion_cantidad: 150,
                usuario_id: userId
            };
            const response = await request(server)
                .post('/food/ali')
                .send(foodData);

            console.log('Response Body:', response.body); 
            expect(response.status).toBe(200);  
            expect(response.body.Status).toBe('Alimentacion guardada');  

        });

        it('PUT /api/food/ali/:alimentacion_id should update an existing food entry and respond with status 200', async () => {
            const updatedFoodData = {
                alimentacion_fecha: '2024-05-13',
                producto_id: productId,
                alimentacion_cantidad: 200,
                usuario_id: userId
            };
            const response = await request(server)
                .put(`/api/food/ali/${alimentacionId}`)
                .send(updatedFoodData);
            expect(response.status).toBe(200);
            expect(response.body.Status).toBe('Alimentacion actualizada');
        }, 10000); 


        it('GET /api/food/ali/all/:alimentacion_id should respond with status 200 and return the food entry with the specified ID', async () => {
            const response = await request(server).get(`/api/food/ali/all/${alimentacionId}`);
            expect(response.status).toBe(200);
            expect(response.body.length).toBe(1);
            expect(response.body[0].alimentacion_id).toBe(alimentacionId);
        });

        it('DELETE /food/ali/:alimentacion_id should delete an existing food entry and respond with status 200', async () => {
            const response = await request(server).delete(`/food/ali/${alimentacionId}`);
            expect(response.status).toBe(200);
            expect(response.body.Status).toBe('Alimentacion eliminada');
        });

        it('DELETE /api/products/:producto_id should delete an existing product and respond with status 200', async () => {
            const response = await request(server).delete(`/api/products/${productId}`);
            expect(response.status).toBe(200);
        });
    });

    afterAll(done => {
        server.close(done);
    }, 10000); 
});