const { Router } = require('express');
const router = Router();

const MySqlConnection = require('../database');

router.get('/', (req, res) => {
    res.setHeader("Gitlab-On-Demand-DAST", "86c8c0a0-3add-463a-bbdc-8f5f20850f10");
    res.status(200).json('Servidor en el puerto 3000 y la DB se conectaron');
});

router.get('/users', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    mysqlConnection.connection.query('select * from tbl_usuario', (error, rows, fields) => {
        if (!error) {
            res.json(rows);
        } else {
            console.log(error);
        }
        mysqlConnection.disconnect();
    });
});

router.get('/users/:usuario_id', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    const { usuario_id } = req.params;
    mysqlConnection.connection.query('select * from tbl_usuario where usuario_id = ?', [usuario_id], (error, rows, fields) => {
        if (!error) {
            res.json(rows);
        } else {
            console.log(error);
        }
        mysqlConnection.disconnect();
    });
});

router.post('/users/login', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    const { usuario_email, usuario_password } = req.body;
    mysqlConnection.connection.query('select usuario_id from tbl_usuario where usuario_email = ? and usuario_password=?', [usuario_email, usuario_password], (error, rows, fields) => {
        if (!error) {
            res.json(rows);
        } else {
            console.log(error);
        }
        mysqlConnection.disconnect();
    });
});

router.post('/users', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    const { usuario_nombre, usuario_email, usuario_password } = req.body;
    mysqlConnection.connection.query('insert into tbl_usuario(usuario_nombre, usuario_email, usuario_password) values (?, ?, ?)', [usuario_nombre, usuario_email, usuario_password], (error, rows, fields) => {
        if (!error) {
            res.json({ Status: "Usuario guardado" })
        } else {
            console.log(error);
        }
        mysqlConnection.disconnect();
    });
});

router.put('/users/:usuario_id', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    const { usuario_id } = req.params;
    const { usuario_nombre, usuario_email, usuario_password } = req.body;
    mysqlConnection.connection.query('update tbl_usuario set usuario_nombre = ?, usuario_email = ?, usuario_password = ? where usuario_id = ?;', [usuario_nombre, usuario_email, usuario_password, usuario_id], (error, rows, fields) => {
        if (!error) {
            res.json({ Status: 'Usuario actualizado' });
        } else {
            console.log(error);
        }
        mysqlConnection.disconnect();
    });
});

router.delete('/users/:usuario_id', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    const { usuario_id } = req.params;
    mysqlConnection.connection.query('delete from tbl_usuario where usuario_id = ?', [usuario_id], (error, rows, fields) => {
        if (!error) {
            res.json({ Status: "Usuario eliminado" });
        } else {
            res.json({ Status: error });
        }
        mysqlConnection.disconnect();
    });
});

module.exports = router;
